# README #

This project was created in order to customize the Django administrative pages with style Semanti ui.

### What is this repository for? ###

* Quick summary
* Version: 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration

## Dependencies ##

1. Django 1.8 (https://www.djangoproject.com/download/)
1. Semantic-ui 2.1.4 (http://semantic-ui.com/)
1. jquery.mask.js (https://github.com/igorescobar/jQuery-Mask-Plugin/blob/master/src/jquery.mask.js)
1. jquery.simple-dtpicker.js (https://github.com/mugifly/jquery-simple-datetimepicker)

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Valberto Carneiro
* http://valberto.com
* valbertovc@gmail.com